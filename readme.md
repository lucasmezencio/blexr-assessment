## Blexr Employee Control

### Requirements

- Docker and Docker Compose (https://www.docker.com/)

### Setup

Clone the project, change directory into project path then run the following commands in sequence:

```
cp .env.example .env
docker-compose up -d
```

After the containers are up and running, run the following in order:

```
docker-compose exec blexr_nginx composer install
docker-compose exec blexr_nginx php artisan key:generate
docker-compose exec blexr_nginx php artisan migrate --seed
```

The project will be running at [http://localhost](http://localhost).

### Misc

As an Admin, you should login using the following credentials:

- email: `admin@blexr.com`
- password: `Bl3xr@2018`

Emails will be sent under a queue to avoid request blocking. To do it, the following command must be run:

`docker-compose exec blexr_nginx php artisan queue:work`

The assets are already compiled, but if any changes are made, the following commands must be run:

- `docker-compose exec blexr_nginx npm install`
- `docker-compose exec blexr_nginx npm run development`
