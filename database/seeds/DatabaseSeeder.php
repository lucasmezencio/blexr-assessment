<?php

use App\Access;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        User::create([
            'name' => 'Administrator',
            'email' => 'admin@blexr.com',
            'password' => bcrypt('Bl3xr@2018'),
            'type' => User::TYPE_ADMIN,
        ]);

        $accesses = [
            'email',
            'git repository',
            'microsoft office license',
            'trello',
        ];

        foreach ($accesses as $access) {
            Access::create([
                'name' => $access,
            ]);
        }
    }
}
