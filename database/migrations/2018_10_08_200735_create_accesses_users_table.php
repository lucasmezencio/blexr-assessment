<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessesUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_user', function (Blueprint $table) {
            $table->unsignedInteger('access_id');
            $table->unsignedInteger('user_id');

            $table->foreign('access_id')->references('id')->on('accesses');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('access_user', function (Blueprint $table) {
            $table->dropForeign('access_user_access_id_foreign');
            $table->dropForeign('access_user_user_id_foreign');
        });

        Schema::dropIfExists('access_user');
    }
}
