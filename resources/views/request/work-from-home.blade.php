@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Work From <i class="fas fa-home"></i>

                        <a href="{{ URL::route('employee') }}" class="btn btn-sm btn-primary float-right">
                            <i class="fas fa-arrow-left"></i> Back
                        </a>
                    </div>

                    <div class="card-body">
                        @include('partials.status')

                        {!! Form::open(['route' => 'request.create', 'method' => 'post']) !!}
                            <div class="row">
                                <div class="form-group col">
                                    <div class="form-check">
                                        {!! Form::checkbox('sick', Request::old('sick'), null, [
                                            'id' => 'sick',
                                            'class' => 'form-check-input',
                                        ]) !!}
                                        {!! Form::label('sick', "I'm sick?", ['class' => 'form-check-label']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col">
                                    {!! Form::label('date', 'When?') !!}
                                    {!! Form::text('date', Request::old('date'), [
                                        'class' => 'datepicker',
                                        'readonly'
                                    ]) !!}
                                </div>

                                <div class="form-group col">
                                    {!! Form::label('hours', 'How Much Time? (hours)') !!}
                                    {!! Form::number('hours', Request::old('hours'), ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    {!! Form::submit('Submit', ['class' => 'btn btn-block btn-success']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
