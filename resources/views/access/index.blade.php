@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Accesses

                        <a href="{{ URL::route('accesses.new') }}" class="btn btn-sm btn-primary float-right">
                            <span class="fas fa-plus"></span> New Access
                        </a>
                    </div>

                    <div class="card-body">
                        @include('partials.status', ['show' => true])

                        <table class="table table-striped" id="accesses">
                            <thead>
                            <tr>
                                <th>Name</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($accesses as $access)
                                <tr>
                                    <td>{{ $access->name }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
