@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        New Access

                        <a href="{{ URL::route('accesses.index') }}" class="btn btn-sm btn-primary float-right">
                            <i class="fas fa-arrow-left"></i> Back
                        </a>
                    </div>

                    <div class="card-body">
                        @include('partials.status')

                        {!! Form::open(['route' => 'accesses.create', 'method' => 'post']) !!}
                            <div class="form-group">
                                {!! Form::label('name', 'Name') !!}
                                {!! Form::text('name', Request::old('name'), [
                                    'class' => 'form-control',
                                    'placeholder' => 'Name',
                                ]) !!}
                            </div>

                            {!! Form::submit('Submit', ['class' => 'btn btn-block btn-success']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
