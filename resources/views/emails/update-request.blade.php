<p>Hey {{ $employee->name }}!</p>
<p></p>
<p>
    Your request to Work from Home on {{ $request->date }}, was @if ($request->approved === 1) approved @else declined @endif.
</p>
<p></p>
<p><em>Blexr Team</em></p>
