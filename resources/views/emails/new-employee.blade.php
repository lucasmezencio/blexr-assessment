<p>Hey {{ $employee->name }}!</p>
<p></p>
<p>Welcome to Blexr!</p>
<p></p>
<p>Here are your credentials:</p>
<p>E-mail: {{ $employee->email }}</p>
<p>Password: <code>{{ $password }}</code></p>
<p></p>
<p><em>Blexr Team</em></p>
