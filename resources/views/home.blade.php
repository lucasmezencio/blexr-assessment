@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="employees-tab" data-toggle="tab" href="#employees"
                                   role="tab"
                                   aria-controls="home" aria-selected="true">Employees</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" id="requests-tab" data-toggle="tab" href="#requests" role="tab"
                                   aria-controls="profile" aria-selected="false">
                                    Work from <i class="fas fa-home"></i> Requests
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="card-body">
                        @include('partials.status')

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="employees" role="tabpanel"
                                 aria-labelledby="employees-tab">
                                <a href="{{ URL::route('employee.new') }}" class="btn btn-sm btn-primary float-right">
                                    <span class="fas fa-plus"></span> Add new Employee
                                </a>

                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>E-mail</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($employees as $employee)
                                        <tr>
                                            <td>
                                                <a href="{{ URL::route('employee.access', ['id' => $employee->id]) }}">
                                                    {{ $employee->name }}
                                                </a>
                                            </td>

                                            <td>{{ $employee->email }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="tab-pane fade" id="requests" role="tabpanel" aria-labelledby="requests-tab">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Employee</th>
                                        <th>When?</th>
                                        <th>How Much Time?</th>
                                        <th>Is Employee Sick?</th>
                                        <th>Approved?</th>
                                    </tr>
                                    </thead>
                                    <tbody id="requests">
                                    @foreach ($requests as $request)
                                        <tr>
                                            <td>{{ $request->user->name }}</td>
                                            <td>{{ $request->date }}</td>
                                            <td>{{ $request->hours }}</td>

                                            <td>
                                                @if ($request->sick)
                                                    Yes
                                                @else
                                                    No
                                                @endif
                                            </td>

                                            <td>
                                                @if ($request->approved === 1)
                                                    <i class="fas fa-check text-success"></i>
                                                @elseif ($request->approved === 0)
                                                    <i class="fas fa-times text-danger"></i>
                                                @else
                                                    <i class="fas fa-circle-notch fa-spin d-none"></i>

                                                    <a href="{{ URL::route('request.approve', [
                                                        'id' => $request->id,
                                                    ]) }}" class="approve" title="Approve">
                                                        <i class="fas fa-check text-success"></i>
                                                    </a>

                                                    <a href="{{ URL::route('request.decline', [
                                                        'id' => $request->id,
                                                    ]) }}" class="decline" title="Decline">
                                                        <i class="fas fa-times text-danger"></i>
                                                    </a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
