@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Hello, <strong>{{ Auth::user()->name }}</strong>!

                    <a href="{{ URL::route('request.new') }}" class="btn btn-sm btn-primary float-right">
                        Work from <i class="fas fa-home"></i>
                    </a>
                </div>

                <div class="card-body">
                    @include('partials.status')

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th colspan="3" class="text-center">
                                Your Requests
                            </th>
                        </tr>
                        <tr>
                            <th>When?</th>
                            <th>How Much Time?</th>
                            <th>Approved?</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach (Auth::user()->requests as $request)
                            <tr>
                                <td>{{ $request->date }}</td>
                                <td>{{ $request->hours }}</td>
                                <td>
                                    @if ($request->approved)
                                        <i class="fas fa-check text-success"></i>
                                    @else
                                        <i class="fas fa-clock"></i>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
