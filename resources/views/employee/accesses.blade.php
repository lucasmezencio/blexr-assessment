@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <strong>{{ $employee->name }}</strong> Accesses

                        <a href="{{ URL::route('home') }}" class="btn btn-sm btn-primary float-right">
                            <i class="fas fa-arrow-left"></i> Back
                        </a>
                    </div>

                    <div class="card-body">
                        @include('partials.status', ['show' => true])

                        {!! Form::open(['url' => '#', 'id' => 'employeeAccesses']) !!}
                            @foreach ($accesses as $access)
                                <div class="form-check form-check-inline">
                                    {!! Form::checkbox(
                                        'accesses[]',
                                        $access->id,
                                        $employee->accesses->contains('id', $access->id),
                                        [
                                            'id' => "access-{$access->id}",
                                            'class' => 'form-check-input',
                                            'data-employee' => $employee->id,
                                        ]
                                    ) !!}
                                    <i class="fas fa-circle-notch fa-spin form-check-input d-none"></i>
                                    {!! Form::label("access-{$access->id}", $access->name, [
                                        'class' => 'form-check-label',
                                    ]) !!}
                                </div>
                            @endforeach
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
