@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Add New Employee

                    <a href="{{ URL::route('home') }}" class="btn btn-sm btn-primary float-right">
                        <i class="fas fa-arrow-left"></i> Back
                    </a>
                </div>

                <div class="card-body">
                    @include('partials.status')

                    {!! Form::open(['route' => 'employee.create', 'method' => 'post']) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Name') !!}
                            {!! Form::text('name', Request::old('name'), [
                                'class' => 'form-control',
                                'placeholder' => 'Name',
                            ]) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'E-mail') !!}
                            {!! Form::text('email', Request::old('email'), [
                                'class' => 'form-control',
                                'placeholder' => 'Name',
                            ]) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('type', 'Name') !!}
                            {!! Form::select('type', [
                                \App\User::TYPE_ADMIN => 'Admin',
                                \App\User::TYPE_USER => 'Employee',
                            ], null, ['class' => 'form-control']) !!}
                        </div>

                        {!! Form::submit('Submit', ['class' => 'btn btn-block btn-success']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
