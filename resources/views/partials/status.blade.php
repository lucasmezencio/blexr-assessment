@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-{{ session('class', 'success') }}" role="alert">
        {{ session('status') }}
    </div>
@endif

@if (isset($show) && $show)
    <div class="alert d-none" role="alert"></div>
@endif
