(function ($) {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content,
    },
  });

  var $employeeAccesses = $('#employeeAccesses');

  if ($employeeAccesses.length) {
    $employeeAccesses.on('click', 'input[type=checkbox]', function (e) {
      var check = this;
      var $alert = $('.alert');
      var accesses = [];

      // Get checked accesses
      $('input[type=checkbox]:checked', $employeeAccesses).each(function (i, el) {
        accesses.push(el.value);
      });

      $.ajax({
        type: 'POST',
        url: laroute.route('employee.access.grant', {
          id: check.dataset.employee,
        }),
        data: {
          accesses: accesses,
        },
        dataType: 'json',
        beforeSend: function () {
          $('input[type=checkbox]', $employeeAccesses).prop('disabled', true);
          $(check).hide().siblings('.fa-circle-notch').removeClass('d-none');
        },
      }).done(function (data) {
        $('input[type=checkbox]', $employeeAccesses).prop('disabled', false);
        $(check).show().siblings('.fa-circle-notch').addClass('d-none');
        $alert.html(data.message).addClass('alert-success').removeClass('d-none');

        // Wait 5 seconds then close the alert
        setTimeout(function () {
          $alert.hide();
        }, 5000);
      }).fail(function (xhr) {
        var data = xhr.responseJSON;

        check.checked = false;

        $('input[type=checkbox]', $employeeAccesses).prop('disabled', false);
        $(check).show().siblings('.fa-circle-notch').addClass('d-none');
        $alert.html(data.message).addClass('alert-danger').removeClass('d-none');
      });
    });
  }

  var $date = $('input.datepicker');

  if ($date.length) {
    var $sick = $('#sick');

    function setMinDate() {
      var now = moment();
      var currentHour = now.hours();
      var sick = $sick.is(':checked');

      // If the employee is sick, a request can be made by 8am of that same work day
      if (sick) {
        if (currentHour < 8) {
          return new Date(now.year(), now.month(), now.date());
        }

        now.add(1, 'd');

        return new Date(now.year(), now.month(), now.date());
      }

      // A request has to be made at least 8hours before the end of the previous day
      if (currentHour > 15) {
        now.add(2, 'd');

        return new Date(now.year(), now.month(), now.date());
      }

      now.add(1, 'd');

      return new Date(now.year(), now.month(), now.date());
    }

    var pickerOptions = {
      uiLibrary: 'bootstrap4',
      minDate: setMinDate,
      format: 'yyyy/mm/dd',
    };

    $date.datepicker(pickerOptions);

    $sick.on('click', function () {
      $date.datepicker(pickerOptions);
    });
  }

  var $requests = $('#requests');

  if ($requests.length) {
    $requests
      .on('click', '.approve', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var _this = this;

        $.ajax({
          type: 'POST',
          url: this.href,
          dataType: 'json',
          beforeSend: function () {
            $('a', $requests).hide();
            $('.fa-circle-notch', $requests).removeClass('d-none');
          }
        }).done(function () {
          $('.fa-circle-notch', $requests).addClass('d-none');
          $('a', $requests).show();
          $('a.decline', $requests).remove();
          $(_this).unbind();
        }).fail(function () {
          $('.fa-circle-notch', $requests).addClass('d-none');
          $('a', $requests).show();
        });
      })
      .on('click', '.decline', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var _this = this;

        $.ajax({
          type: 'POST',
          url: this.href,
          dataType: 'json',
          beforeSend: function () {
            $('a', $requests).hide();
            $('.fa-circle-notch', $requests).removeClass('d-none');
          }
        }).done(function () {
          $('.fa-circle-notch', $requests).addClass('d-none');
          $('a', $requests).show();
          $('a.approve', $requests).remove();
          $(_this).unbind();
        }).fail(function () {
          $('.fa-circle-notch', $requests).addClass('d-none');
          $('a', $requests).show();
        });
      });
  }
})(window.jQuery);
