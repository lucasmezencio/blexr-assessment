<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Lord\Laroute\LarouteServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    private $devProviders = [
        LarouteServiceProvider::class
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            foreach ($this->devProviders as $provider) {
                $this->app->register($provider);
            }
        }
    }
}
