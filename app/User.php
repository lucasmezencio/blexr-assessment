<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @package App
 *
 * @property bool $type
 */
class User extends Authenticatable
{
    use Notifiable;

    public const TYPE_ADMIN = 1;
    public const TYPE_USER = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return BelongsToMany
     */
    public function accesses(): BelongsToMany
    {
        return $this->belongsToMany(Access::class);
    }

    /**
     * @return HasMany
     */
    public function requests(): HasMany
    {
        return $this->hasMany(Request::class);
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->type;
    }

    /**
     * @param Builder $builder
     *
     * @return Builder
     */
    public function scopeEmployee(Builder $builder): Builder
    {
        return $builder->whereType(self::TYPE_USER);
    }
}
