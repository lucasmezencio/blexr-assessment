<?php

namespace App\Http\Controllers;

use Faker\Generator;
use Illuminate\Contracts\View\View as ViewContract;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Mail;
use Redirect;
use Response;
use View;

use App\Access;
use App\Http\Requests\UserRequest;
use App\Mail\NewEmployeeMail;
use App\User;

class EmployeeController extends Controller
{
    /** @var Generator */
    private $faker;

    /**
     * EmployeeController constructor.
     *
     * @param Generator $faker
     */
    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }

    /**
     * @return ViewContract
     */
    public function new(): ViewContract
    {
        return View::make('employee.new');
    }

    /**
     * @param UserRequest $request
     *
     * @return RedirectResponse
     */
    public function create(UserRequest $request): RedirectResponse
    {
        try {
            $password = $this->faker->password;
            $employee = new User($request->all());
            $employee->password = bcrypt($password);
            $employee->save();

            // Enqueue e-mail, the asynchronous the better
            Mail::to($employee->email)->queue(new NewEmployeeMail($employee, $password));

            return Redirect::route('home')->with('status', 'Employee created');
        } catch (\Exception $e) {
            return Redirect::route('employee.new')
                ->withInput()
                ->with('status', 'An error occurred, please try again.')
                ->with('class', 'danger');
        }
    }

    /**
     * @param int $id
     *
     * @return ViewContract|RedirectResponse
     */
    public function access(int $id)
    {
        try {
            $employee = User::findOrFail($id);
            $accesses = Access::all();

            return View::make('employee.accesses', compact('employee', 'accesses'));
        } catch (ModelNotFoundException $e) {
            return Redirect::route('home')
                ->with('status', 'Employee not found.')
                ->with('class', '');
        }
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function grant(Request $request, int $id): JsonResponse
    {
        try {
            /** @var User $employee */
            $employee = User::findOrFail($id);
            $accesses = $request->get('accesses');

            $employee->accesses()->sync($accesses);
            $employee->save();
            $employee->refresh();

            return Response::json([
                'status' => true,
                'message' => 'Accesses granted.',
                'accesses' => $employee->accesses()->pluck('id')->toArray(),
            ]);
        } catch (ModelNotFoundException $e) {
            return Response::json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 404);
        } catch (\Exception $e) {
            return Response::json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 500);
        }
    }

    public function employee()
    {
        return View::make('employee.index');
    }
}
