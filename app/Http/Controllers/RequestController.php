<?php

namespace App\Http\Controllers;

use App\Mail\UpdateRequestMail;
use Auth;
use Illuminate\Contracts\View\View as ViewContract;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Mail;
use Redirect;
use Response;
use View;

use App\Http\Requests\RequestRequest;
use App\Request;

class RequestController extends Controller
{
    /**
     * @return ViewContract
     */
    public function workFromHome(): ViewContract
    {
        return View::make('request.work-from-home');
    }

    /**
     * @param RequestRequest $request
     *
     * @return RedirectResponse
     */
    public function request(RequestRequest $request): RedirectResponse
    {
        try {
            Auth::user()->requests()->create($request->all());

            return Redirect::route('employee')
                ->with('status', 'Request waiting for approval.');
        } catch (\Exception $e) {
            return Redirect::back()
                ->with('status', "An error occurred. ({$e->getMessage()})")
                ->with('class', 'danger')
                ->withInput();
        }
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function approve(int $id): JsonResponse
    {
        return $this->handleRequest($id, true);
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function decline(int $id): JsonResponse
    {
        return $this->handleRequest($id, false);
    }

    /**
     * @param int $id
     * @param bool $status
     *
     * @return JsonResponse
     */
    private function handleRequest(int $id, bool $status): JsonResponse
    {
        try {
            $request = Request::findOrFail($id);
            $request->approved = $status;
            $request->save();

            $employee = $request->user;

            // Enqueue e-mail, the asynchronous the better
            Mail::to($employee->email)->queue(new UpdateRequestMail($employee, $request));

            return Response::json([
                'status' => true,
                'message' => 'Done',
            ]);
        } catch (ModelNotFoundException $e) {
            return Response::json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 404);
        }
    }
}
