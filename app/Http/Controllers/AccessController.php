<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View as ViewContract;
use Illuminate\Http\RedirectResponse;
use Redirect;
use View;

use App\Access;
use App\Http\Requests\AccessRequest;

/**
 * Class AccessController
 *
 * @package App\Http\Controllers
 */
class AccessController extends Controller
{
    /**
     * @return ViewContract
     */
    public function index(): ViewContract
    {
        $accesses = Access::all();

        return View::make('access.index', compact('accesses'));
    }

    /**
     * @return ViewContract
     */
    public function new(): ViewContract
    {
        return View::make('access.new');
    }

    /**
     * @param AccessRequest $request
     *
     * @return RedirectResponse
     */
    public function create(AccessRequest $request): RedirectResponse
    {
        Access::create($request->all());

        return Redirect::route('accesses.index')
            ->with('status', 'Access created');
    }
}
