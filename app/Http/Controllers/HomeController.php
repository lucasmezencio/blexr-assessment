<?php

namespace App\Http\Controllers;

use App\Request;
use Illuminate\Contracts\View\View as ViewContract;
use View;

use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return ViewContract
     */
    public function index(): ViewContract
    {
        $employees = User::employee()->orderBy(User::CREATED_AT, 'DESC')->get();
        $requests = Request::all();

        return View::make('home', compact('employees', 'requests'));
    }
}
