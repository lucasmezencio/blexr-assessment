<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $userTypes = implode(',', [
            User::TYPE_USER,
            User::TYPE_ADMIN,
        ]);

        return [
            'name' => 'required|string|min:2',
            'email' => 'required|email',
            'type' => "required|in:{$userTypes}",
        ];
    }
}
