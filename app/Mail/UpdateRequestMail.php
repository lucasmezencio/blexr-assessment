<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Request;
use App\User;

class UpdateRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    /** @var User */
    private $employee;
    /** @var Request */
    private $request;

    /**
     * Create a new message instance.
     *
     * @param User $employee
     * @param bool $request
     */
    public function __construct(User $employee, Request $request)
    {
        $this->employee = $employee;
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->view('emails.update-request', [
            'employee' => $this->employee,
            'request' => $this->request,
        ]);
    }
}
