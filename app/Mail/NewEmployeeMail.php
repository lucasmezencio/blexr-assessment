<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;

class NewEmployeeMail extends Mailable
{
    use Queueable, SerializesModels;

    /** @var string */
    private $employee;
    /** @var string */
    private $password;

    /**
     * Create a new message instance.
     *
     * @param User $employee
     * @param string $password
     */
    public function __construct(User $employee, string $password)
    {
        $this->employee = $employee;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this->view('emails.new-employee', [
            'employee' => $this->employee,
            'password' => $this->password,
        ]);
    }
}
