<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $user = Auth::user();

    if ($user) {
        if ($user->isAdmin()) {
            return Redirect::route('home');
        }

        return Redirect::route('employee');
    }

    return View::make('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'prefix' => 'accesses',
    'middleware' => ['auth', 'admin'],
], function () {
    Route::get('/', [
        'as' => 'accesses.index',
        'uses' => 'AccessController@index',
    ]);

    Route::get('/new', [
        'as' => 'accesses.new',
        'uses' => 'AccessController@new',
    ]);

    Route::post('/new', [
        'as' => 'accesses.create',
        'uses' => 'AccessController@create',
    ]);
});

Route::get('/new-employee', [
    'as' => 'employee.new',
    'uses' => 'EmployeeController@new',
])->middleware(['auth', 'admin']);

Route::post('/new-employee', [
    'as' => 'employee.create',
    'uses' => 'EmployeeController@create',
])->middleware(['auth', 'admin']);

Route::group([
    'prefix' => 'employee-accesses/{id}',
    'middleware' => ['auth', 'admin'],
], function () {
    Route::get('/', [
        'as' => 'employee.access',
        'uses' => 'EmployeeController@access',
    ]);

    Route::post('/grant', [
        'as' => 'employee.access.grant',
        'uses' => 'EmployeeController@grant',
    ]);
});

Route::get('/employee', [
    'as' => 'employee',
    'uses' => 'EmployeeController@employee',
]);

Route::group(['prefix' => 'request'], function () {
    Route::get('/work-from-home', [
        'as' => 'request.new',
        'uses' => 'RequestController@workFromHome',
    ]);

    Route::post('/work-from-home', [
        'as' => 'request.create',
        'uses' => 'RequestController@request',
    ]);

    Route::group(['prefix' => '{id}'], function () {
        Route::post('/approve', [
            'as' => 'request.approve',
            'uses' => 'RequestController@approve',
        ]);

        Route::post('/decline', [
            'as' => 'request.decline',
            'uses' => 'RequestController@decline',
        ]);
    });
});
